//package Utils;
//
//import net.thucydides.core.webdriver.DriverSource;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
//
//public class Util implements DriverSource {
//    public WebDriver newDriver() {
////        String videoFake        = "--use-file-for-fake-video-capture=" + rootDir + "\\src\\test\\resources\\files\\fake_webcam.y4m";
////        String pathDownload     = rootDir + "\\src\\test\\resources\\downloads";
//
//        try {
////            System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/windows/chromedriver93.exe");
//
////            HashMap<String, Object> chromePreferences = new HashMap<>();
////            chromePreferences.put("download.default_directory", pathDownload);
//
////            ChromeOptions options = new ChromeOptions();
////            options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
////            options.addArguments("--enable-extentions");
////            //            options.addArguments("user-data-dir=C:\\Users\\Admin\\AppData\\Local\\Google\\Chrome\\User Data\\Default");
////            options.setExperimentalOption("excludeSwitches", Arrays.asList("test-type"));
////            options.setExperimentalOption("prefs", chromePreferences);
////            //            options.addArguments("--disable-application-cache");
////            options.addArguments("--disable-site-isolation-trials");
////            options.addArguments("use-fake-ui-for-media-stream");
////            options.addArguments("use-fake-device-for-media-stream");
////            options.addArguments("ignore-certificate-errors");
//
//            ChromeOptions handlingSSL = new ChromeOptions();
//            handlingSSL.setAcceptInsecureCerts(true);
//            handlingSSL.addArguments("--no-sandbox;--ignore-certificate-errors;--homepage=about:blank;--no-first-run");
//
////            DesiredCapabilities capabilities = new DesiredCapabilities();
////            capabilities.setCapability(ChromeOptions.CAPABILITY, handlingSSL);;
////            handlingSSL.merge(capabilities);
//
//            return new ChromeDriver(handlingSSL);
//        } catch (Exception e) {
//            throw new Error(e);
//        }
//    }
//
//    @Override
//    public boolean takesScreenshots() {
//        return false;
//    }
//
//    @Override
//    public Class<? extends WebDriver> driverType() {
//        return DriverSource.super.driverType();
//    }
//}
