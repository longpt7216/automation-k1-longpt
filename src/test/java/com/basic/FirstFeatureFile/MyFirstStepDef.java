package com.basic.FirstFeatureFile;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MyFirstStepDef {
	
	WebDriver driver;
	
	@Given("^User need to be on Facebook login page$")
	public void user_need_to_be_on_Facebook_login_page() {
		String tag = System.getProperty("driver");
		switch (tag){
			case "chrome" :
				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();
				break;
			case "firefox" :
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
				break;
			case "edge" :
				WebDriverManager.edgedriver().setup();
				driver = new EdgeDriver();
				break;
			default:
				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();
				break;
		}
		driver.get("https://www.facebook.com/");
	}
	
	@When("^User enters user first name$")

	public void user_enters_user_first_name() {
		String msg = System.getProperty("driver");
		driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys(msg);
		driver.quit();
	}

	@Then("^User checks user first name is present$")
	public void user_checks_use_first_name_is_present() {
		String userNameActual = driver.findElement(By.xpath("//*[@id=\"email\"]")).getAttribute("value");
		Assert.assertEquals(userNameActual, "longpt");
	}
}
